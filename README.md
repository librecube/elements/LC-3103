# LibreEquipment EGSE Flatsat

A simple breadboard that allows parallel mounting of CubeSat boards and
provides full access to all the pins of the CubeSat connector.

![](docs/images/main.jpg)

## Getting Started

Find the design source files in the `source` folder. All information and files
needed for manufacturing are in `build` folder.
