EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Flatsat Board"
Date "Di 09 Jun 2015"
Rev "0"
Comp "LibreCube"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L flatsat_board-rescue:CONN_02X26-flatsat_board-rescue-flatsat_board-rescue P1_1
U 1 1 5577BB67
P 4700 3550
F 0 "P1_1" H 4700 4900 50  0000 C CNN
F 1 "CONN_02X26" V 4700 3550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x26" H 4700 2850 60  0001 C CNN
F 3 "" H 4700 2850 60  0000 C CNN
	1    4700 3550
	1    0    0    -1  
$EndComp
$Comp
L flatsat_board-rescue:CONN_02X26-flatsat_board-rescue-flatsat_board-rescue P1_2
U 1 1 5577BC9C
P 4700 3550
F 0 "P1_2" H 4700 4900 50  0000 C CNN
F 1 "CONN_02X26" V 4700 3550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x26" H 4700 2850 60  0001 C CNN
F 3 "" H 4700 2850 60  0000 C CNN
	1    4700 3550
	1    0    0    -1  
$EndComp
$Comp
L flatsat_board-rescue:CONN_02X26-flatsat_board-rescue-flatsat_board-rescue H1_1
U 1 1 5577BE94
P 4700 3550
F 0 "H1_1" H 4700 4900 50  0000 C CNN
F 1 "CONN_02X26" V 4700 3550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x26" H 4700 2850 60  0001 C CNN
F 3 "" H 4700 2850 60  0000 C CNN
	1    4700 3550
	1    0    0    -1  
$EndComp
$Comp
L flatsat_board-rescue:CONN_02X26-flatsat_board-rescue-flatsat_board-rescue H1_2
U 1 1 5577BE9A
P 4700 3550
F 0 "H1_2" H 4700 4900 50  0000 C CNN
F 1 "CONN_02X26" V 4700 3550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x26" H 4700 2850 60  0001 C CNN
F 3 "" H 4700 2850 60  0000 C CNN
	1    4700 3550
	1    0    0    -1  
$EndComp
$Comp
L flatsat_board-rescue:CONN_02X26-flatsat_board-rescue-flatsat_board-rescue H1_3
U 1 1 5577CB14
P 4700 3550
F 0 "H1_3" H 4700 4900 50  0000 C CNN
F 1 "CONN_02X26" V 4700 3550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x26" H 4700 2850 60  0001 C CNN
F 3 "" H 4700 2850 60  0000 C CNN
	1    4700 3550
	1    0    0    -1  
$EndComp
$Comp
L flatsat_board-rescue:CONN_02X26-flatsat_board-rescue-flatsat_board-rescue H1_4
U 1 1 5577CBB7
P 4700 3550
F 0 "H1_4" H 4700 4900 50  0000 C CNN
F 1 "CONN_02X26" V 4700 3550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x26" H 4700 2850 60  0001 C CNN
F 3 "" H 4700 2850 60  0000 C CNN
	1    4700 3550
	1    0    0    -1  
$EndComp
$Comp
L flatsat_board-rescue:CONN_02X26-flatsat_board-rescue-flatsat_board-rescue P2_1
U 1 1 5577CE7D
P 4700 6300
F 0 "P2_1" H 4700 7650 50  0000 C CNN
F 1 "CONN_02X26" V 4700 6300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x26" H 4700 5600 60  0001 C CNN
F 3 "" H 4700 5600 60  0000 C CNN
	1    4700 6300
	1    0    0    -1  
$EndComp
$Comp
L flatsat_board-rescue:CONN_02X26-flatsat_board-rescue-flatsat_board-rescue P2_2
U 1 1 5577CE83
P 4700 6300
F 0 "P2_2" H 4700 7650 50  0000 C CNN
F 1 "CONN_02X26" V 4700 6300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x26" H 4700 5600 60  0001 C CNN
F 3 "" H 4700 5600 60  0000 C CNN
	1    4700 6300
	1    0    0    -1  
$EndComp
$Comp
L flatsat_board-rescue:CONN_02X26-flatsat_board-rescue-flatsat_board-rescue H2_1
U 1 1 5577CE89
P 4700 6300
F 0 "H2_1" H 4700 7650 50  0000 C CNN
F 1 "CONN_02X26" V 4700 6300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x26" H 4700 5600 60  0001 C CNN
F 3 "" H 4700 5600 60  0000 C CNN
	1    4700 6300
	1    0    0    -1  
$EndComp
$Comp
L flatsat_board-rescue:CONN_02X26-flatsat_board-rescue-flatsat_board-rescue H2_2
U 1 1 5577CE8F
P 4700 6300
F 0 "H2_2" H 4700 7650 50  0000 C CNN
F 1 "CONN_02X26" V 4700 6300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x26" H 4700 5600 60  0001 C CNN
F 3 "" H 4700 5600 60  0000 C CNN
	1    4700 6300
	1    0    0    -1  
$EndComp
$Comp
L flatsat_board-rescue:CONN_02X26-flatsat_board-rescue-flatsat_board-rescue H2_3
U 1 1 5577CE95
P 4700 6300
F 0 "H2_3" H 4700 7650 50  0000 C CNN
F 1 "CONN_02X26" V 4700 6300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x26" H 4700 5600 60  0001 C CNN
F 3 "" H 4700 5600 60  0000 C CNN
	1    4700 6300
	1    0    0    -1  
$EndComp
$Comp
L flatsat_board-rescue:CONN_02X26-flatsat_board-rescue-flatsat_board-rescue H2_4
U 1 1 5577CE9B
P 4700 6300
F 0 "H2_4" H 4700 7650 50  0000 C CNN
F 1 "CONN_02X26" V 4700 6300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x26" H 4700 5600 60  0001 C CNN
F 3 "" H 4700 5600 60  0000 C CNN
	1    4700 6300
	1    0    0    -1  
$EndComp
$EndSCHEMATC
